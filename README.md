## Photo Manager

### Background

Photo manager will take in two paramters, one source and one destination directory on the filesystem.
For each file in the directory it will try and extract the date from the .exif information
and rename the file following the ISO 8601 timeformat (a slight modified truth) and if there
are multiple files with the exact same date it will try to pick the photo with the highest resolution.

The usage and design of the code is basic, there are definitely room for many improvements.

I have used the code for what I needed for now, so no further development is planned,
but requests will be considered, or by all means I'm happy for anyone to contribute or fork
as deemed fit.

#### Reasoning

This is a private project to help me organise my private photographs.

Over the years I have had photos from multiple devices, each naming the photos in different ways.
One in particular, one of my Xperia phones always start naming photos from 0.
This works fine until you photos off from the device, and take another one, now also named img_0.

Also a bit lazily I had multiple copies of some photos, so some were genuine duplicates
and some were just colliding due to the name.

I think you could reach the same or similar functionality by usage of the command line
and some tools, but it was an excuse for me to write something in Kotlin.

## Usage

There is no assembled .jar file. I ran the code right from the IDE with the necessary inputs.