package hgyllensvard.photomanager.file.move

import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.FileExistsException
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileMover @Inject constructor(
    private val fileService: FileService
) {

    fun appenToDestPathAndMove(sourceFile: File, destFile: File, path: String): Single<MoveFileResult> {
        return fileService.appendDirPathAtEnd(destFile, path)
            .flatMap { moveFileTo(sourceFile, it) }
    }

    fun moveFileTo(source: File, dest: File, options: FileMoveOptions = FileMoveOptions()): Single<MoveFileResult> {
        return Single.fromCallable({ Pair(source, dest) })
            .subscribeOn(Schedulers.io())
            .doOnSuccess { fileService.assertIsFile(source) }
            .doOnSuccess { fileService.assertIsFileIfExist(dest) }
            .flatMap { updateDestFileIfNecessary(dest, options) }
            .map {
                val renameResult = source.renameTo(it)
                MoveFileResult(renameResult, source, it)
            }
            .onErrorReturn { MoveFileResult(false, source, dest, it) }
    }

    private fun updateDestFileIfNecessary(dest: File, options: FileMoveOptions): Single<File> {
        if (dest.exists()) {
            if (options.createIndexIfFileExist) {
                return findNewDestFile(dest)
            } else {
                throw FileExistsException(dest.absolutePath)
            }
        }

        return Single.just(dest)
    }

    private fun findNewDestFile(destFile: File): Single<File> {
        return fileService.indexFileNameIfDuplicates(destFile)
            .map {
                when {
                    it.success -> return@map it.newFile
                    it.error != null -> throw it.error
                    else -> throw RuntimeException("Cannot move file to %s, but the reason is unknown".format(destFile))
                }
            }
    }
}