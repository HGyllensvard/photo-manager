package hgyllensvard.photomanager.file.move

import java.io.File

data class MoveFilesResult(
    val success: Boolean,
    val sourcePath: File,
    val destPath: File,
    val emptySource: Boolean,
    val failures: List<File>,
    val failCount: Int = failures.size,
    val error: Throwable? = null)
