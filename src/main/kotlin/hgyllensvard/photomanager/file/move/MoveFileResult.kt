package hgyllensvard.photomanager.file.move

import java.io.File

data class MoveFileResult(
    val success: Boolean,
    val sourceFile: File,
    val destFile: File,
    val error: Throwable? = null)