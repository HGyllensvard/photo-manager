package hgyllensvard.photomanager.file.move

import hgyllensvard.photomanager.file.photo.PhotoInformationService
import hgyllensvard.photomanager.file.rename.PhotoPathService
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import java.io.File

class FileResolutionResolver(
    private val photoInformationService: PhotoInformationService,
    private val fileService: FileService,
    private val fileMover: FileMover,
    private val photoPathService: PhotoPathService
) {
    fun manageDifferentHash(sourceFile: File, destFile: File): Single<MoveFileResult> {
        return photoInformationService.isHigherResolution(sourceFile, destFile)
            .flatMap {
                when (it) {
                    1, 0 -> {
                        return@flatMap fileService.appendDirPathAtEnd(destFile, photoPathService.duplicateDir())
                            .flatMap { fileMover.moveFileTo(destFile, it) }
                            .flatMap { fileMover.moveFileTo(sourceFile, destFile) }
                    }
                    else -> {
                        return@flatMap moveSourceToDuplicate(destFile, sourceFile)
                    }
                }
            }
    }

    private fun moveSourceToDuplicate(destFile: File, sourceFile: File): Single<MoveFileResult> {
        return fileService.appendDirPathAtEnd(destFile, photoPathService.duplicateDir())
            .flatMap { fileMover.moveFileTo(sourceFile, it) }
    }
}