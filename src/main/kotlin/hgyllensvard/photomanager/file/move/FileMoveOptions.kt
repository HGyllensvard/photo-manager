package hgyllensvard.photomanager.file.move

data class FileMoveOptions(
    val createIndexIfFileExist: Boolean = true)