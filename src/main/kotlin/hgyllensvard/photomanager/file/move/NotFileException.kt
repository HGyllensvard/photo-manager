package hgyllensvard.photomanager.file.move

import java.io.IOException

class NotFileException(errorMessage: String) : IOException(errorMessage)
