package hgyllensvard.photomanager.file.rename

enum class PhotoActonResult {
    SUCCESS,
    NO_DATE_INFORMATION,
    DUPLICATED_NAME,
    UNKNOWN_ERROR,
    SECURITY_EXCEPTION
}