package hgyllensvard.photomanager.file.rename

import java.io.File

data class RenameResult(
    val renamedFiles: List<File> = ArrayList(),
    val failedRenaming: List<SingleRenameResult> = ArrayList())