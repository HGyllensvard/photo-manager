package hgyllensvard.photomanager.file.rename

import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoPathService @Inject constructor() {

    companion object {
        const val unknownDir = "unknown"
        const val noDateDir = "noDateInfo"
        const val duplicatedName = "duplicatedName"
    }

    fun unknownDir(): String = unknownDir
    fun noDateDir(): String = noDateDir
    fun duplicateDir(): String = duplicatedName
}