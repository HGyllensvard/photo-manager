package hgyllensvard.photomanager.file.rename

import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.move.MoveFileResult
import io.reactivex.Single
import org.apache.logging.log4j.LogManager
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RenameService @Inject constructor(
    private val fileMover: FileMover,
    private val sameHashService: SameHashService,
    private val photoComparisonService: PhotoComparisonService
) {
    private val logger = LogManager.getLogger()

    fun renameFile(sourceFile: File, destFile: File): Single<MoveFileResult> {
        return if (destFile.exists()) {
            sameHashService.manageIfSameHash(sourceFile, destFile)
                .flatMap {
                    if (it.success) {
                        logger.info("Duplicate file found: %s, moved file to: %s", it.sourceFile, it.destFile)
                        Single.just(it)
                    } else {
                        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
                    }
                }
        } else {
            logger.info("Moving: %s to: %s", sourceFile, destFile)
            destFile.parentFile.mkdirs()
            fileMover.moveFileTo(sourceFile, destFile)
        }
    }
}