package hgyllensvard.photomanager.file.rename

import java.io.File

data class SingleRenameResult(
    val file: File,
    val renameResult: PhotoActonResult,
    val throwable: Throwable? = null)