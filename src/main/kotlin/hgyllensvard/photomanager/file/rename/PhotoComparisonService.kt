package hgyllensvard.photomanager.file.rename

import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.move.MoveFileResult
import hgyllensvard.photomanager.file.photo.PhotoInformationService
import io.reactivex.Single
import org.apache.logging.log4j.LogManager
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.log

@Singleton
class PhotoComparisonService @Inject constructor(
    private val photoInformationService: PhotoInformationService,
    private val photoPathService: PhotoPathService,
    private val fileMover: FileMover
) {

    private val logger = LogManager.getLogger()

    fun keepLargestResolutionFile(sourceFile: File, destFile: File): Single<MoveFileResult> {
        return photoInformationService.isHigherResolution(sourceFile, destFile)
            .flatMap {
                val duplicatedDestPath = photoPathService.duplicateDir()
                when (it) {
                    1, 0 -> {
                        return@flatMap replaceSourceWithDest(sourceFile, destFile, duplicatedDestPath)
                    }
                    else -> {
                        logger.info("Moving source: %s to duplicate folder", sourceFile)
                        return@flatMap fileMover.appenToDestPathAndMove(sourceFile, destFile, duplicatedDestPath)
                    }
                }
            }.onErrorReturn { MoveFileResult(false, sourceFile, destFile, it) }

    }

    private fun replaceSourceWithDest(sourceFile: File, destFile: File, duplicatedDestPath: String): Single<MoveFileResult>? {
        logger.info("Keeping source: %s over dest: %s because it got higher resolution", sourceFile, destFile)
        return fileMover.appenToDestPathAndMove(sourceFile, destFile, duplicatedDestPath)
            .flatMap {
                if (!it.success) {
                    Single.just(it)
                } else {
                    fileMover.moveFileTo(sourceFile, destFile)
                }
            }
    }
}