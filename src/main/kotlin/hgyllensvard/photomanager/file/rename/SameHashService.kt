package hgyllensvard.photomanager.file.rename

import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.move.MoveFileResult
import hgyllensvard.photomanager.util.file.HashService
import io.reactivex.Single
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SameHashService @Inject constructor(
    private val hashService: HashService,
    private val photoPathService: PhotoPathService,
    private val fileMover: FileMover) {

    fun manageIfSameHash(sourceFile: File, destFile: File): Single<MoveFileResult> {
        return hashService.sameHash(sourceFile, destFile)
            .flatMap {
                if (it) {
                    return@flatMap fileMover.appenToDestPathAndMove(sourceFile, destFile, photoPathService.duplicateDir())
                } else {
                    Single.just(MoveFileResult(false, sourceFile, destFile))
                }
            }
    }
}