package hgyllensvard.photomanager.file.photo

data class PhotoSizeResult(
    val success: Boolean = false,
    val value: Int = INVALID_VALUE,
    val error: Throwable? = null) {

    companion object {
        const val INVALID_VALUE = -1
    }
}