package hgyllensvard.photomanager.file.photo

import java.time.LocalDateTime

data class PhotoDateResult(
    val success: Boolean = false,
    val date: LocalDateTime? = null)