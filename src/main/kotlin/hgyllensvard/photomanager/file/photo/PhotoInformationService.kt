package hgyllensvard.photomanager.file.photo

import com.drew.imaging.ImageMetadataReader
import com.drew.metadata.exif.ExifDirectoryBase
import com.drew.metadata.exif.ExifIFD0Directory
import hgyllensvard.photomanager.util.DateConverterService
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.awt.Rectangle
import java.io.File
import java.util.*
import javax.inject.Inject

class PhotoInformationService @Inject constructor(
    private val dateConverterService: DateConverterService
) {
    fun extractPhotoDate(file: File): Single<PhotoDateResult> {
        return readMetadata(file)
            .observeOn(Schedulers.computation())
            .map {
                it.directories
                    .asSequence()
                    .filter { it.containsTag(ExifIFD0Directory.TAG_DATETIME) }
                    .map { it.getDate(ExifIFD0Directory.TAG_DATETIME, TimeZone.getDefault()) }
                    .filter { it != null }
                    .map { dateConverterService.dateToLocalTime(it) }
                    .forEach { return@map PhotoDateResult(true, it) }

                PhotoDateResult(false, null)
            }
            .onErrorReturn { PhotoDateResult(false, null) }
            .subscribeOn(Schedulers.io())
    }

    fun extractPhotoWidth(file: File): Single<PhotoSizeResult> {
        return getSize(file, ExifDirectoryBase.TAG_EXIF_IMAGE_WIDTH)
    }

    fun extractPhotoHeight(file: File): Single<PhotoSizeResult> {
        return getSize(file, ExifDirectoryBase.TAG_EXIF_IMAGE_HEIGHT)
    }

    fun extractSizeOfPhoto(file: File): Single<Rectangle> {
        return Single.zip(extractPhotoWidth(file), extractPhotoHeight(file),
            BiFunction { widthResult, heightResult ->
                Rectangle(widthResult.value, heightResult.value)
            })
    }

    fun extractMegaPixels(file: File): Single<Int> {
        return extractSizeOfPhoto(file)
            .map { it.height * it.width }
    }

    fun isHigherResolution(fileOne: File, fileTwo: File): Single<Int> {
        return Single.zip(
            extractMegaPixels(fileOne),
            extractMegaPixels(fileTwo),
            BiFunction { fileOneMegaPixels, fileTwoMegaPixels ->
                when {
                    fileOneMegaPixels == fileTwoMegaPixels -> 0
                    fileOneMegaPixels >= fileTwoMegaPixels -> 1
                    else -> -1
                }
            })
    }

    private fun getSize(file: File, tagId: Int): Single<PhotoSizeResult> {
        return readMetadata(file)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .map {
                it.directories
                    .asSequence()
                    .filter { it.containsTag(tagId) }
                    .map { it.getInt(tagId) }
                    .forEach { return@map PhotoSizeResult(true, it) }
                PhotoSizeResult()
            }
            .onErrorReturn { PhotoSizeResult(error = it) }
    }

    private fun readMetadata(file: File) = Single.fromCallable { ImageMetadataReader.readMetadata(file) }
}