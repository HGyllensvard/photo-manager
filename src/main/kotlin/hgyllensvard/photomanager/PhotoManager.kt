package hgyllensvard.photomanager

import hgyllensvard.photomanager.commandparser.CommandParser
import hgyllensvard.photomanager.commandparser.validate.ArgumentValidator
import hgyllensvard.photomanager.commandparser.validate.ValidatedArguments
import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.rename.RenameService
import hgyllensvard.photomanager.photo.PhotoOrchestratorService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.apache.logging.log4j.LogManager
import javax.inject.Inject

class PhotoManager {

    private val logger = LogManager.getLogger()

    @Inject
    lateinit var commandParser: CommandParser

    @Inject
    lateinit var argumentValidator: ArgumentValidator

    @Inject
    lateinit var photoOrchestratorService: PhotoOrchestratorService

    init {
        PhotoManagerInjector.photoManagerComponent()
            .inject(this)
    }

    fun start(args: Array<String>): Single<ValidatedArguments> {
        return commandParser.parseArguments(args)
            .flatMap { argumentValidator.validateArguments(it) }
            .doOnSuccess { logger.info("Validated arguments: %s".format(it)) }
            .doOnSuccess {
                photoOrchestratorService.parseInput(it)
                    .doOnSuccess { logger.debug("Parsed input with result: %s".format(it)) }
            }
            .subscribeOn(Schedulers.computation())
    }
}