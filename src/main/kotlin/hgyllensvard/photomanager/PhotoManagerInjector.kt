package hgyllensvard.photomanager

import hgyllensvard.photomanager.di.DaggerPhotoManagerComponent
import hgyllensvard.photomanager.di.PhotoManagerComponent


object PhotoManagerInjector {

    private val photoManagerComponent = DaggerPhotoManagerComponent
        .builder()
        .build()

    fun photoManagerComponent(): PhotoManagerComponent = photoManagerComponent
}