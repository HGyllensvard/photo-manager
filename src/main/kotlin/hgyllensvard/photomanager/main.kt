package hgyllensvard.photomanager

import io.reactivex.schedulers.Schedulers
import org.apache.logging.log4j.LogManager


fun main(args: Array<String>) {
    println("Starting Photo Manager")

    val logger = LogManager.getLogger()

    val photoManager = PhotoManager()

    println("Parsing arguments")

    photoManager.start(args = args)
        .observeOn(Schedulers.computation())
        .doOnSuccess({ commandArg -> logger.info(commandArg) })
        .doOnError({ throwable -> logger.error(throwable) })
        // Without a blocking get the java process proceeds and is then killed not guarantee a result.
        .blockingGet()
}
