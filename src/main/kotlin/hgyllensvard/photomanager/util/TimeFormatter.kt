package hgyllensvard.photomanager.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TimeFormatter @Inject constructor() {

    /**
     * As some file system does not allow naming with :, we strip them to have HH.MM.SS
     */
    fun toFileSystemFriendlyISO8601Name(localTime: LocalDateTime) =
        localTime.format(DateTimeFormatter.ISO_DATE_TIME)
            .replace(":", ".")
}

