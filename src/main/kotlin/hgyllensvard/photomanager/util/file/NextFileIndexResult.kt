package hgyllensvard.photomanager.util.file

import java.io.File

data class NextFileIndexResult(
    val success: Boolean = true,
    val newFile: File,
    val error: Throwable? = null)