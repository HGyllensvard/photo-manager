package hgyllensvard.photomanager.util.file

import hgyllensvard.photomanager.exceptions.BlankInputException
import hgyllensvard.photomanager.file.move.NotFileException
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.file.NotDirectoryException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileService @Inject constructor() {

    companion object {
        const val maxItems = 1000
    }

    fun indexFileNameIfDuplicates(destFile: File): Single<NextFileIndexResult> {
        return Single.fromCallable({
            val destDir = destFile.parentFile

            var index = 1
            var targetDestFile = destFile

            while (targetDestFile.exists() && index < maxItems) {
                if (index >= maxItems) {
                    val error = IllegalStateException("Too many attempts to move file")
                    return@fromCallable NextFileIndexResult(false, destFile, error)
                }

                val extension = destFile.extension
                val fileEnding = if (extension.isNotBlank()) "." + extension else ""
                val indexedFileName = destFile.nameWithoutExtension + "_" + index + fileEnding
                targetDestFile = File(destDir, indexedFileName)
                index++
            }

            NextFileIndexResult(newFile = targetDestFile)
        }).subscribeOn(Schedulers.io())
    }

    /**
     * Recursively finds all files in the specified directory.
     */
    fun filesInDir(file: File): Collection<File> {
        return FileUtils.listFiles(file, null, true)
    }

    fun validatePathIsDir(sourcePath: String): Single<File> {
        if (sourcePath.isBlank()) {
            return Single.error(BlankInputException("sourcePath"))
        }

        val sourceFile = File(sourcePath)

        if (isDir(sourceFile)) {
            return Single.just(sourceFile)
        }

        return Single.error(NotDirectoryException(sourcePath))
    }

    private fun isDir(sourceFile: File) = sourceFile.exists() && sourceFile.isDirectory

    fun appendDirPathAtEnd(destFile: File, newPath: String): Single<File> {
        return Single.just(File(destFile.parentFile, File(newPath, destFile.name).path))
    }

    @Throws(NotFileException::class)
    fun assertIsFileIfExist(file: File) {
        if (file.exists() && file.isDirectory) {
            throw NotFileException(file.absolutePath)
        }
    }

    @Throws(NotFileException::class)
    fun assertIsFile(file: File) {
        if ((!file.exists()) || !file.isFile) {
            throw NotFileException(file.absolutePath)
        }
    }

    @Throws(NotDirectoryException::class)
    fun assertIsDir(dir: File) {
        if (!dir.exists() || !dir.isDirectory) {
            throw NotDirectoryException(dir.absolutePath)
        }
    }

    fun getFiles(source: File): Array<out File> = source.listFiles() ?: arrayOf()

    // It's quite annoying to the file.extension, therefor easy to wrap it here
    fun extension(file: File): String {
        return file.extension
    }
}