package hgyllensvard.photomanager.util.file

import hgyllensvard.photomanager.util.file.hash.GetHashResult
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.apache.commons.codec.digest.DigestUtils
import java.io.File
import java.io.FileInputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HashService @Inject constructor() {

    /**
     * If the SHA calculation fails for any reason the
     * hash will be a blank string and the error part of the result.
     */
    fun getSha256(file: File): Single<GetHashResult> {
        return Single.fromCallable {
            try {
                val fis = FileInputStream(file)
                val sha256 = DigestUtils.sha256Hex(fis)
                fis.close()
                GetHashResult(true, sha256)
            } catch (e: Exception) {
                GetHashResult(false, "", e)
            }
        }.subscribeOn(Schedulers.io())
    }

    fun sameHash(fileOne: File, fileTwo: File): Single<Boolean> {
        return Single.zip(getSha256(fileOne), getSha256(fileTwo),
            BiFunction { resultOne, resultTwo ->
                if (resultOne.error != null) {
                    throw resultOne.error
                }

                if (resultTwo.error != null) {
                    throw resultTwo.error
                }

                if (resultOne.hash.isBlank() || resultTwo.hash.isBlank()) {
                    return@BiFunction false
                }

                resultOne.hash == resultTwo.hash
            })
    }
}