package hgyllensvard.photomanager.util.file.hash

data class GetHashResult(
    val success: Boolean = true,
    val hash: String,
    val error: Throwable? = null)