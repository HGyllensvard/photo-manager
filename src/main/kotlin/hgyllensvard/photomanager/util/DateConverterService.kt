package hgyllensvard.photomanager.util

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

class DateConverterService {

    fun dateToLocalTime(date: Date): LocalDateTime {
        val instant = Instant.ofEpochMilli(date.time)
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
    }
}
