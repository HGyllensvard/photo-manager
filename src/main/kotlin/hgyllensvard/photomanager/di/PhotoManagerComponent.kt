package hgyllensvard.photomanager.di

import dagger.Component
import hgyllensvard.photomanager.PhotoManager
import javax.inject.Singleton

@Singleton
@Component(modules = [PhotoManagerModule::class])
interface PhotoManagerComponent {

    fun inject(photoManager: PhotoManager)

}