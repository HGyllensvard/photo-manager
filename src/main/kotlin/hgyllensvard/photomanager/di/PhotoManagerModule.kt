package hgyllensvard.photomanager.di

import dagger.Module
import dagger.Provides
import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.util.DateConverterService
import javax.inject.Singleton

@Module
class PhotoManagerModule {

    @Provides
    @Singleton
    fun dateConverterService(): DateConverterService = DateConverterService()

}