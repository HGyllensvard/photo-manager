package hgyllensvard.photomanager.photo

import hgyllensvard.photomanager.file.rename.PhotoActonResult
import java.io.File

data class DestNameResult(
    val success: Boolean = true,
    val sourceFile: File,
    val destFile: File,
    val actionResult: PhotoActonResult)