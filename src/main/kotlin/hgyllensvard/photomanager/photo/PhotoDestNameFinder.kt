package hgyllensvard.photomanager.photo

import hgyllensvard.photomanager.file.rename.PhotoActonResult
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

class PhotoDestNameFinder @Inject constructor(
    private val photoNameFinder: PhotoNameFinder) {

    fun findDestFile(sourceFile: File, destDir: File): Single<DestNameResult> {
        return photoNameFinder.findPhotoName(sourceFile)
            .map { name ->
                if (name.isBlank()) {
                    return@map DestNameResult(false, sourceFile, File(""), PhotoActonResult.NO_DATE_INFORMATION)
                } else {
                    val destFile = buildFile(destDir, name)
                    return@map DestNameResult(true, sourceFile, destFile, PhotoActonResult.SUCCESS)
                }
            }
            .subscribeOn(Schedulers.io())
    }

    private fun buildFile(destDir: File, name: String): File {
        return File(destDir.parentFile, name)
    }
}