package hgyllensvard.photomanager.photo

import hgyllensvard.photomanager.file.photo.PhotoInformationService
import hgyllensvard.photomanager.util.TimeFormatter
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import java.io.File
import java.time.LocalDateTime
import javax.inject.Inject

class PhotoNameFinder @Inject constructor(
    private val photoInformationService: PhotoInformationService,
    private val timeFormatter: TimeFormatter,
    private val fileService: FileService) {

    /**
     * Build the name based upon the input file, or an empty string if
     * no name could be found
     */
    fun findPhotoName(file: File): Single<String> {
        return photoInformationService.extractPhotoDate(file)
            .map { (success, localTime) ->
                if (success && localTime != null) assembleName(file, localTime) else ""
            }
    }

    private fun assembleName(file: File, localTime: LocalDateTime): String {
        val extension = fileService.extension(file)
        val iso8601Name = timeFormatter.toFileSystemFriendlyISO8601Name(localTime)
        return "%s.%s".format(iso8601Name, extension)
    }
}