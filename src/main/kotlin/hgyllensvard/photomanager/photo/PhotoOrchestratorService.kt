package hgyllensvard.photomanager.photo

import hgyllensvard.photomanager.commandparser.validate.ValidatedArguments
import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.move.MoveFileResult
import hgyllensvard.photomanager.file.rename.RenameService
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoOrchestratorService @Inject constructor(
    private val fileService: FileService,
    private val renameService: RenameService,
    private val photoDestNameFinder: PhotoDestNameFinder) {

    fun parseInput(arguments: ValidatedArguments): Single<List<MoveFileResult>> {
        val allFiles = fileService.filesInDir(arguments.sourceDir)

        return Observable.fromIterable(allFiles)
            .flatMap { photoDestNameFinder.findDestFile(it, arguments.destDir).toObservable() }
            .flatMap({
                if (!it.success) {
                    return@flatMap Observable.error<MoveFileResult>(IllegalStateException("Could not find a destination folder forÖ %s".format(it.sourceFile)))
                } else {
                    return@flatMap renameService.renameFile(it.sourceFile, it.destFile)
                        .toObservable()
                }
            }).toList()
    }
}