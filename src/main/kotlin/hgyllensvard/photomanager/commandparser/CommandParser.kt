package hgyllensvard.photomanager.commandparser

import io.reactivex.Single
import org.apache.logging.log4j.LogManager
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.Option
import org.kohsuke.args4j.CmdLineParser
import org.kohsuke.args4j.ParserProperties
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommandParser @Inject constructor() {

    private val logger = LogManager.getLogger()

    companion object {
        const val sourceOptionIdentifier = "-s"
        const val destOptionIdentifier = "-d"
    }

    @Option(name = sourceOptionIdentifier, required = true, usage = "Source directory")
    private lateinit var sourceDir: String

    @Option(name = destOptionIdentifier, required = true, usage = "Destination directory")
    private lateinit var destinationDir: String

    private val commandParser = CmdLineParser(this)

    fun parseArguments(args: Array<String>): Single<CommandArguments> {
        try {
            commandParser.parseArgument(*args)
        } catch (e: CmdLineException) {
            return Single.error(e)
        }

        val parsedArguments = CommandArguments(sourceDir, destinationDir)

        logger.info("Arguments parsed: {}", parsedArguments)
        return Single.just(parsedArguments)
    }
}
