package hgyllensvard.photomanager.commandparser

data class CommandArguments(
    val sourcePath: String,
    val destinationPath: String)