package hgyllensvard.photomanager.commandparser.validate

import java.io.File

data class ValidatedArguments(
    val sourceDir: File,
    val destDir: File)