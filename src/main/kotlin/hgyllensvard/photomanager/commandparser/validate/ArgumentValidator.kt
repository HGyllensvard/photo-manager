package hgyllensvard.photomanager.commandparser.validate

import hgyllensvard.photomanager.commandparser.CommandArguments
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArgumentValidator @Inject constructor(
    private val fileService: FileService
) {
    fun validateArguments(commandArguments: CommandArguments): Single<ValidatedArguments> {
        val sourceSingle = fileService.validatePathIsDir(commandArguments.sourcePath)
        val destSingle = fileService.validatePathIsDir(commandArguments.destinationPath)

        return Single.zip(sourceSingle, destSingle, BiFunction { sourceFile, destResult ->
            ValidatedArguments(sourceFile, destResult)
        })
    }
}