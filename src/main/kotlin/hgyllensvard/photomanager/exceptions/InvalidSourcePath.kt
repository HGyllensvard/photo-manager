package hgyllensvard.photomanager.exceptions

class BlankInputException(variable: String = "") : IllegalArgumentException("Input provided was blank for %s".format(variable))