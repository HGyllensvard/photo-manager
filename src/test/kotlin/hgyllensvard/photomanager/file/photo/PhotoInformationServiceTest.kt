package hgyllensvard.photomanager.file.photo

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.util.DateConverterService
import hgyllensvard.photomanager.utils.testPhotoWithDate
import hgyllensvard.photomanager.utils.testPhotoWithoutDate
import org.junit.Before
import org.junit.Test

import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.awt.Rectangle

class PhotoInformationServiceTest {

    private val dateConverterService: DateConverterService = mock()

    @InjectMocks
    lateinit var photoInformationService: PhotoInformationService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun extractPhotoDate() {
        val testPhoto = testPhotoWithDate()

        whenever(dateConverterService.dateToLocalTime(any())).thenReturn(testPhoto.expectedDate)

        photoInformationService.extractPhotoDate(testPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(PhotoDateResult(true, testPhoto.expectedDate))
    }

    @Test
    fun extractPhotoDate_withoutDate() {
        val testPhoto = testPhotoWithoutDate()

        photoInformationService.extractPhotoDate(testPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(PhotoDateResult(false, null))
    }

    @Test
    fun extractPhotoHeight() {
        val testPhoto = testPhotoWithDate()

        photoInformationService.extractPhotoHeight(testPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(PhotoSizeResult(true, testPhoto.height))
    }

    @Test
    fun extractPhotoWidth() {
        val testPhoto = testPhotoWithDate()

        photoInformationService.extractPhotoWidth(testPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(PhotoSizeResult(true, testPhoto.width))
    }

    @Test
    fun extractPhotoSize() {
        val testPhoto = testPhotoWithDate()

        photoInformationService.extractSizeOfPhoto(testPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(Rectangle(testPhoto.width, testPhoto.height))
    }

    @Test
    fun testHighestResolution() {
        val biggerTestPhoto = testPhotoWithDate()
        val smallerTestPhoto = testPhotoWithoutDate()

        photoInformationService.isHigherResolution(biggerTestPhoto.file, smallerTestPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(1)

        photoInformationService.isHigherResolution(smallerTestPhoto.file, biggerTestPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(-1)
    }

    @Test
    fun testHigherResolution_samePhoto() {
        val biggerTestPhoto = testPhotoWithDate()

        photoInformationService.isHigherResolution(biggerTestPhoto.file, biggerTestPhoto.file)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(0)
    }
}