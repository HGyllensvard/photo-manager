package hgyllensvard.photomanager.file.move

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.util.file.FileService
import hgyllensvard.photomanager.util.file.NextFileIndexResult
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.apache.commons.io.FileExistsException
import org.junit.Before
import org.junit.Test

import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.io.File
import java.nio.file.NotDirectoryException
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class FileMoverTest {

    private val sourceFile: File = mock()
    private val destFile: File = mock()
    private val newDestFile: File = mock()

    private val fileService: FileService = mock()

    private lateinit var fileMover: FileMover

    private val sourcePath = "sourcePath"
    private val destPath = "destPath"
    private val newDestPath = "newDestPath"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mockFileExist(sourceFile, sourcePath)
        mockFileExist(destFile, destPath, false)
        mockFileExist(newDestFile, newDestPath, true)

        fileMover = FileMover(fileService)
    }

    @Test
    fun moveFile_noSourceFile() {
        whenever(fileService.assertIsFile(sourceFile)).thenThrow(NotFileException("not a file"))
        assertMoveFileFailure()
    }

    @Test
    fun moveFile_destFileExist_isNotFile() {
        whenever(fileService.assertIsFileIfExist(destFile)).thenThrow(NotFileException("not a file"))

        val result = fileMover.moveFileTo(sourceFile, destFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .values()[0]

        assertFalse { result.success }
        assertTrue { result.error is NotFileException }
    }

    @Test
    fun moveFileCreateIndexedFile() {
        val indexResult = NextFileIndexResult(true, newDestFile)

        whenever(fileService.indexFileNameIfDuplicates(any())).thenReturn(Single.just(indexResult))
        whenever(sourceFile.renameTo(newDestFile)).thenReturn(true)
        whenever(destFile.exists()).thenReturn(true)

        fileMover.moveFileTo(sourceFile, destFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue(MoveFileResult(true, sourceFile, newDestFile))
    }

    @Test
    fun moveFileCreateIndexedFile_failure() {
        val error = RuntimeException("error")
        val indexResult = NextFileIndexResult(false, destFile, error)

        whenever(fileService.indexFileNameIfDuplicates(any())).thenReturn(Single.just(indexResult))
        whenever(sourceFile.renameTo(destFile)).thenReturn(true)
        whenever(destFile.exists()).thenReturn(true)

        fileMover.moveFileTo(sourceFile, destFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue(MoveFileResult(false, sourceFile, destFile, error))
    }

    @Test
    fun moveFile_destExist_doNotIndexFile() {
        val indexResult = NextFileIndexResult(false, newDestFile)

        whenever(fileService.indexFileNameIfDuplicates(any())).thenReturn(Single.just(indexResult))
        whenever(sourceFile.renameTo(newDestFile)).thenReturn(true)
        whenever(destFile.exists()).thenReturn(true)

        val result = fileMover.moveFileTo(sourceFile, destFile, FileMoveOptions(false))
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .values()[0]

        assertFalse { result.success }
        assertTrue { result.error is FileExistsException }
    }

    @Test
    fun moveFile_failRename() {
        verifyFileMove(false)
    }

    @Test
    fun moveFile_successfulRename() {
        verifyFileMove(true)
    }

    private fun assertMoveFileFailure() {
        val result = fileMover.moveFileTo(sourceFile, destFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .values()[0]

        assertFalse { result.success }
        assertTrue { result.error is NotFileException }
    }

    private fun verifyFileMove(success: Boolean) {
        whenever(sourceFile.renameTo(destFile)).thenReturn(success)

        fileMover.moveFileTo(sourceFile, destFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue(MoveFileResult(success, sourceFile, destFile))
    }

    private fun mockFileExist(file: File, path: String, exist: Boolean = true) {
        whenever(file.absolutePath).thenReturn(path)
        whenever(file.exists()).thenReturn(exist)
        whenever(file.isFile).thenReturn(true)
    }
}
