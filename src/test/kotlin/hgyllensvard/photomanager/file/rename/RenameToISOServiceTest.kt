//package hgyllensvard.photomanager.file.rename
//
//import com.nhaarman.mockitokotlin2.any
//import com.nhaarman.mockitokotlin2.mock
//import com.nhaarman.mockitokotlin2.whenever
//import hgyllensvard.photomanager.file.photo.PhotoDateResult
//import hgyllensvard.photomanager.file.photo.PhotoInformationService
//import io.reactivex.Single
//import org.junit.Before
//import org.junit.Test
//
//import org.mockito.InjectMocks
//import org.mockito.MockitoAnnotations
//import java.io.File
//import java.time.LocalDateTime
//
//
//class RenameToISOServiceTest {
//
//    private val photoInformationService: PhotoInformationService = mock()
//    private val mockedDir: File = mock()
//
//
//    @InjectMocks
//    lateinit var renameToISOService: RenameToISOService
//
//    @Before
//    fun setUp() {
//        MockitoAnnotations.initMocks(this)
//
//        whenever(mockedDir.listFiles()).thenReturn(arrayOf(mockedFile))
//        whenever(mockedDir.isDirectory).thenReturn(true)
//        whenever(mockedDir.isFile).thenReturn(false)
//
//    }
//
//    @Test
//    fun noFiles_returnEmptyResult() {
//        whenever(mockedDir.listFiles()).thenReturn(emptyArray())
//
//        assertRenameResult(RenameResult(emptyList(), emptyList()))
//    }
//
//
//
//    @Test
//    fun securityException_failRename() {
//        mockFileContainsPhotoDate(true, localTime)
//
//        val exception = SecurityException()
//        whenever(mockedFile.renameTo(any())).thenThrow(exception)
//
//        assertRenameResult(RenameResult(emptyList(), listOf(SingleRenameResult(mockedFile, PhotoActonResult.SECURITY_EXCEPTION, throwable = exception))))
//    }
//
//    @Test
//    fun failRename_unknownError() {
//        mockFileContainsPhotoDate(true, localTime)
//
//        whenever(mockedFile.renameTo(any())).thenReturn(false)
//
//        assertRenameResult(RenameResult(emptyList(), listOf(SingleRenameResult(mockedFile, PhotoActonResult.UNKNOWN_ERROR))))
//    }
//
//    @Test
//    fun successfulExtractInformationAndRename() {
//        mockFileContainsPhotoDate(true, localTime)
//
//        val testPath = File("parentPath")
//        whenever(mockedFile.parentFile).thenReturn(testPath)
//        whenever(mockedFile.renameTo(any())).thenReturn(true)
//
//        assertRenameResult(RenameResult(listOf(mockedFile), emptyList()))
//    }
//
//
//
//    private fun assertRenameResult(renameResult: RenameResult) {
//        renameToISOService.renameToISO8601(mockedDir)
//            .test()
//            .assertNoErrors()
//            .awaitCount(1)
//            .assertValue(renameResult)
//    }
//}