package hgyllensvard.photomanager.file.rename

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.file.move.FileMover
import hgyllensvard.photomanager.file.move.MoveFileResult
import hgyllensvard.photomanager.file.photo.PhotoInformationService
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.io.File
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

class PhotoComparisonServiceTest {

    private val destDir = "destDir"

    private val photoInformationService: PhotoInformationService = mock()
    private val photoPathService: PhotoPathService = mock()
    private val fileMover: FileMover = mock()

    private val sourceFile: File = mock()
    private val destFile: File = mock()
    private val movedToFile: File = mock()

    @InjectMocks
    lateinit var photoComparisonService: PhotoComparisonService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        whenever(photoPathService.duplicateDir()).thenReturn(destDir)
    }

    @Test
    fun moveSmallestFileToDuplicte_failReadResolution() {
        val exception = RuntimeException()

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(1))
        whenever(photoPathService.duplicateDir()).thenThrow(exception)

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitDone(100, TimeUnit.MILLISECONDS)
            .assertValue(MoveFileResult(false, sourceFile, destFile, exception))
    }

    @Test
    fun moveSmallestFileToDuplicte_sourceLargest_failMoveDest() {
        val moveFileResult = MoveFileResult(false, sourceFile, movedToFile)

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(1))
        whenever(fileMover.appenToDestPathAndMove(sourceFile, destFile, destDir)).thenReturn(Single.just(moveFileResult))

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitCount(1)
            .assertValue(moveFileResult)

        verify(fileMover).appenToDestPathAndMove(sourceFile, destFile, destDir)
        verifyNoMoreInteractions(fileMover)
    }

    @Test
    fun moveSmallestFileToDuplicte_sourceLargest_failMoveSource() {
        val moveDest = MoveFileResult(true, sourceFile, movedToFile)
        val moveSource = MoveFileResult(false, sourceFile, movedToFile)

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(1))
        whenever(fileMover.appenToDestPathAndMove(sourceFile, destFile, destDir)).thenReturn(Single.just(moveDest))
        whenever(fileMover.moveFileTo(sourceFile, destFile)).thenReturn(Single.just(moveSource))

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitCount(1)
            .assertValue(moveSource)

        verify(fileMover).appenToDestPathAndMove(sourceFile, destFile, destDir)
        verify(fileMover).moveFileTo(sourceFile, destFile)
        verifyNoMoreInteractions(fileMover)
    }

    @Test
    fun moveSmallestFileToDuplicte_sourceLargest_succeedMoveBoth() {
        val moveDest = MoveFileResult(true, sourceFile, movedToFile)
        val moveSource = MoveFileResult(true, sourceFile, movedToFile)

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(1))
        whenever(fileMover.appenToDestPathAndMove(sourceFile, destFile, destDir)).thenReturn(Single.just(moveDest))
        whenever(fileMover.moveFileTo(sourceFile, destFile)).thenReturn(Single.just(moveSource))

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitCount(1)
            .assertValue(moveSource)

        verify(fileMover).appenToDestPathAndMove(sourceFile, destFile, destDir)
        verify(fileMover).moveFileTo(sourceFile, destFile)
        verifyNoMoreInteractions(fileMover)

    }

    @Test
    fun moveSmallestFileToDuplicte_equalSize() {
        val moveFileResult = MoveFileResult(false, sourceFile, movedToFile)

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(0))
        whenever(fileMover.appenToDestPathAndMove(sourceFile, destFile, destDir)).thenReturn(Single.just(moveFileResult))

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitCount(1)
            .assertValue(moveFileResult)

        verify(fileMover).appenToDestPathAndMove(sourceFile, destFile, destDir)
        verifyNoMoreInteractions(fileMover)

    }

    @Test
    fun moveSmallestFileToDuplicte_destLargest() {
        val moveFileResult = MoveFileResult(false, sourceFile, movedToFile)

        whenever(photoInformationService.isHigherResolution(sourceFile, destFile)).thenReturn(Single.just(-1))
        whenever(fileMover.appenToDestPathAndMove(sourceFile, destFile, destDir)).thenReturn(Single.just(moveFileResult))

        photoComparisonService.keepLargestResolutionFile(sourceFile, destFile)
            .test()
            .awaitCount(1)
            .assertValue(moveFileResult)

        verify(fileMover).appenToDestPathAndMove(sourceFile, destFile, destDir)
        verifyNoMoreInteractions(fileMover)
    }
}