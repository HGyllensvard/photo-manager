package hgyllensvard.photomanager.photo

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.file.photo.PhotoDateResult
import hgyllensvard.photomanager.file.photo.PhotoInformationService
import hgyllensvard.photomanager.util.TimeFormatter
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations

import java.io.File
import java.time.LocalDateTime
import kotlin.system.exitProcess

class PhotoNameFinderTest {

    private val photoInformationService: PhotoInformationService = mock()
    private val timeFormatter: TimeFormatter = mock()
    private val fileService: FileService = mock()

    private val mockedFile: File = mock()
    private val imageExtension = "jpg"
    private val textTime = "2007-12-03T10.15.30"
    private val localTime: LocalDateTime = LocalDateTime.parse(textTime.replace(".", ":"))

    @InjectMocks
    private lateinit var photoNameFinder: PhotoNameFinder

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        
        whenever(fileService.extension(mockedFile)).thenReturn(imageExtension)
        whenever(mockedFile.isFile).thenReturn(true)
        whenever(mockedFile.isDirectory).thenReturn(false)
    }

    @Test
    fun findPhotoName_noPhotoInformation() {
        mockFileContainsPhotoDate(false, null)

        photoNameFinder.findPhotoName(mockedFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue("")
    }

    @Test
    fun findPhotoName_photoInformation_validName() {
        mockFileContainsPhotoDate(true, localTime)
        whenever(timeFormatter.toFileSystemFriendlyISO8601Name(localTime)).thenReturn(textTime)

        photoNameFinder.findPhotoName(mockedFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue("%s.%s".format(textTime, imageExtension))
    }

    private fun mockFileContainsPhotoDate(success: Boolean, date: LocalDateTime?) {
        whenever(photoInformationService.extractPhotoDate(mockedFile)).thenReturn(Single.just(PhotoDateResult(success, date)))
    }
}