package hgyllensvard.photomanager.photo

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.file.rename.PhotoActonResult
import io.reactivex.Single
import org.junit.Test

import java.io.File

class PhotoDestNameFinderTest {

    private val destFileName = "destFileName"
    private val destPath = "destPath"

    private val photoNameFinder: PhotoNameFinder = mock()
    private val sourceFile: File = mock()
    private val destDir: File = mock()

    private val photoDestNameFinder = PhotoDestNameFinder(photoNameFinder)

    @Test
    fun successfulExtractInformationAndRename() {
        whenever(photoNameFinder.findPhotoName(sourceFile)).thenReturn(Single.just(destFileName))
        whenever(destDir.parentFile).thenReturn(File(destPath))

        val expectedFile = File(destPath, destFileName)

        photoDestNameFinder.findDestFile(sourceFile, destDir)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(DestNameResult(true, sourceFile, expectedFile, PhotoActonResult.SUCCESS))
    }

    @Test
    fun noDateInformation() {
        whenever(photoNameFinder.findPhotoName(sourceFile)).thenReturn(Single.just(""))

        photoDestNameFinder.findDestFile(sourceFile, destDir)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(DestNameResult(false, sourceFile, File(""), PhotoActonResult.NO_DATE_INFORMATION))
    }
}