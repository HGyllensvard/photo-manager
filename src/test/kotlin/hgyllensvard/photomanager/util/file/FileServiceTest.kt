package hgyllensvard.photomanager.util.file

import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.io.File

class FileServiceTest {

    private val fileNameWithoutExtension = "fileNameWithoutExtension"
    private val fileName = "testName.jpg"
    private val fileNameIndexOne = "testName_1.jpg"
    private val fileNameIndexTwo = "testName_2.jpg"

    private val testFileWithoutExtension = File(fileNameWithoutExtension)
    private val testFile = File(fileName)
    private val testFileIndexOne = File(fileNameIndexOne)
    private val testFileIndexTwo = File(fileNameIndexTwo)

    @InjectMocks
    lateinit var fileService: FileService

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun teardown() {
        testFileWithoutExtension.deleteRecursively()
        testFile.deleteRecursively()
        testFileIndexOne.deleteRecursively()
        testFileIndexTwo.deleteRecursively()
    }

    @Test
    fun indexFileNameIfDuplicates_fileDoesNotExist_sameFileReturned() {
        assertFileDoesNotExist(testFile)

        checkResultFile(fileName)
    }

    @Test
    fun indexFileNameIfDuplicates_oneFileExist_returnIndexOne() {
        assertFileExist(testFile)
        assertFileDoesNotExist(testFileIndexOne)

        checkResultFile(fileNameIndexOne)
    }

    @Test
    fun indexFileNameIfDuplicates_oneFileExist_noExtensionName_returnIndexOne() {
        assertFileExist(testFileWithoutExtension)

        val result = fileService.indexFileNameIfDuplicates(testFileWithoutExtension)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .values()[0]

        assertTrue(result.success)
        assertEquals(fileNameWithoutExtension + "_1", result.newFile.name)
    }

    @Test
    fun appendPath_onlyFile() {
        val testPath = "test"

        fileService.appendDirPathAtEnd(testFile, testPath)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(File(testPath, fileName))
    }

    @Test
    fun appendPath() {
        val somePath = "somePath"
        val fileName = "fileName.jpg"
        val file = "$somePath/$fileName"
        val testPath = "test"

        fileService.appendDirPathAtEnd(File(file), testPath)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(File("$somePath/$testPath", fileName))
    }

    @Test
    fun indexFileNameIfDuplicates_multipleFilesExist_returnIndexTwo() {
        assertFileExist(testFile)
        assertFileExist(testFileIndexOne)
        assertFileDoesNotExist(testFileIndexTwo)

        checkResultFile(fileNameIndexTwo)
    }


    private fun checkResultFile(expectedFileName: String) {
        val result = fileService.indexFileNameIfDuplicates(testFile)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .values()[0]

        assertTrue(result.success)
        assertEquals(expectedFileName, result.newFile.name)
    }

    private fun assertFileDoesNotExist(file: File) {
        if (file.exists()) {
            assertTrue(testFileIndexTwo.delete())
        }
    }

    private fun assertFileExist(file: File) {
        if (!file.exists()) {
            file.writeText("SomeData")
        }

        assertTrue(file.exists())
    }
}
