package hgyllensvard.photomanager.util.file

import hgyllensvard.photomanager.exceptions.BlankInputException
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.io.File
import java.nio.file.NotDirectoryException

class FileServiceTestPathIsDir {

    private val dirPath = "dirPath"
    private val testDir = File(dirPath)

    private val filePath = "filePath"
    private val testFile = File(filePath)

    @InjectMocks
    lateinit var fileService: FileService

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        testDir.mkdir()
        testFile.writeText("testText")
    }

    @After
    fun teardown() {
        testDir.deleteRecursively()
        testFile.deleteRecursively()
    }

    @Test
    fun validatePathIsDir_emptyPath() {
        fileService.validatePathIsDir("")
            .test()
            .assertError({ it is BlankInputException })
    }

    @Test
    fun validatePathIsDir_isNotDir() {
        fileService.validatePathIsDir(filePath)
            .test()
            .assertError({ it is NotDirectoryException })
    }

    @Test
    fun validatePathIsDir_isDir() {
        fileService.validatePathIsDir(dirPath)
            .test()
            .assertNoErrors()
            .awaitCount(1)
            .assertValue(testDir)
    }
}