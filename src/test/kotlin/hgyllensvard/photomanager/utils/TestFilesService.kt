package hgyllensvard.photomanager.utils

import java.io.File
import java.time.LocalDateTime

private const val testFilesPath = "/src/test/assets/image"
private val projectPath = System.getProperty("user.dir")

private val testFilesDir = File(projectPath, testFilesPath)

fun testPhotoWithDate(): TestPhoto {
    val expectedTime = LocalDateTime.of(2010,
        7, 23, 20, 38, 57)

    return TestPhoto(
        File(testFilesDir.absolutePath, "2010-07-23_20.38.57_withDate.jpg"),
        true,
        2560,
        1920,
        expectedTime)
}

fun testPhotoWithoutDate(): TestPhoto {
    return TestPhoto(
        File(testFilesDir.absolutePath, "photoOneWithoutDate.jpg"),
        false,
        299,
        168,
        null)
}
