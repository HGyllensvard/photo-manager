package hgyllensvard.photomanager.utils

import java.io.File
import java.time.LocalDateTime

data class TestPhoto(
    val file: File,
    val hasDate: Boolean,
    val width: Int,
    val height: Int,
    val expectedDate: LocalDateTime?)
