package hgyllensvard.photomanager.commandparser.validate

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import hgyllensvard.photomanager.commandparser.CommandArguments
import hgyllensvard.photomanager.exceptions.BlankInputException
import hgyllensvard.photomanager.util.file.FileService
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.io.File

class ArgumentValidatorTest {

    private val sourcePath = "sourcePath"
    private val destPath = "destPath"

    private val sourceFile: File = File(sourcePath)
    private val destFile: File = File(destPath)

    private val fileService: FileService = mock()

    @InjectMocks
    private lateinit var argumentValidator: ArgumentValidator

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mockValidPath()
    }

    private fun mockValidPath() {
        whenever(fileService.validatePathIsDir(sourcePath)).thenReturn(Single.just(sourceFile))
        whenever(fileService.validatePathIsDir(destPath)).thenReturn(Single.just(destFile))
    }

    @Test
    fun validateArguments_validArguments() {
        argumentValidator
            .validateArguments(CommandArguments(sourcePath, destPath))
            .test()
            .awaitCount(1)
            .assertValue(ValidatedArguments(File(sourcePath), File(destPath)))
    }

    @Test
    fun validateArguments_sourceNotValid() {
        mockBlankInput(sourcePath)

        assertBlankInputException()
    }

    @Test
    fun validateArguments_destNotValid() {
        mockBlankInput(destPath)

        assertBlankInputException()
    }

    private fun mockBlankInput(path: String) {
        whenever(fileService.validatePathIsDir(path)).thenReturn(Single.error(BlankInputException()))
    }

    private fun assertBlankInputException() {
        argumentValidator
            .validateArguments(CommandArguments(sourcePath, destPath))
            .test()
            .awaitCount(1)
            .assertError({ it is BlankInputException })
    }
}