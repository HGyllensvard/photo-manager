package hgyllensvard.photomanager.commandparser

import org.junit.Before
import org.junit.Test

import org.kohsuke.args4j.CmdLineException

class CommandParserTest {

    private val sourcePath = "sourcePath"
    private val destPath = "destPath"

    lateinit var commandParser: CommandParser

    @Before
    fun setUp() {
        commandParser = CommandParser()
    }

    @Test
    fun parseArgumentNoArguments() = assertCmdLineException(arrayOf())

    @Test
    fun parseArgumentSourcePathMissing() =
        assertCmdLineException(arrayOf(CommandParser.destOptionIdentifier, destPath))

    @Test
    fun parseArgumentDestPathMissing() =
        assertCmdLineException(arrayOf(CommandParser.sourceOptionIdentifier, sourcePath))

    @Test
    fun parseArguments_validArguments() {
        val args = arrayOf(
            CommandParser.sourceOptionIdentifier, sourcePath,
            CommandParser.destOptionIdentifier, destPath)

        commandParser.parseArguments(args = args)
            .test()
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(CommandArguments(sourcePath, destPath))
    }

    private fun assertCmdLineException(args: Array<String>) {
        commandParser.parseArguments(args)
            .test()
            .awaitCount(1)
            .assertError({ it is CmdLineException })
    }
}
